package net.eksb.obsdc

import java.io.File
import java.util.Properties
import java.util.concurrent.CountDownLatch
import kotlin.concurrent.thread

val HOME:File = System.getProperty("user.home")?.let(::File) ?: error("No user.home")
val CONFIG_HOME:File = System.getenv("XDG_CONFIG_HOME")?.let(::File) ?: File(HOME, ".config")
val CONFIG_FILE:File = File(CONFIG_HOME,"net.eksb.obsdc/config.properties")

fun File.properties(): Properties = Properties()
	.also { properties ->
		if (isFile) {
			inputStream().use(properties::load)
		}
	}

fun addShutdownHook(block:()->Unit) = Runtime.getRuntime().addShutdownHook(thread(start=false) { block() })

fun waitForShutdown() {
	val latch = CountDownLatch(1)
	addShutdownHook {
		latch.countDown()
	}
	latch.await()
}