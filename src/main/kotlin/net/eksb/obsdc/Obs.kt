package net.eksb.obsdc

import io.obswebsocket.community.client.OBSRemoteController
import io.obswebsocket.community.client.WebSocketCloseCode
import io.obswebsocket.community.client.listener.lifecycle.ReasonThrowable
import java.util.concurrent.BlockingQueue
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread
import org.slf4j.LoggerFactory
import java.util.concurrent.LinkedBlockingQueue
import kotlin.time.Duration

/**
 * Wrapper for an [OBSRemoteController] that handles connecting, reconnecting, and queuing operations
 * until ready.
 *
 * Call [submit] to submit a request.
 *
 * protocol docs: https://github.com/obsproject/obs-websocket/blob/master/docs/generated/protocol.md
 */
class Obs(
	host:String = "localhost",
	port:Int = 4455,
	password:String,
	connectionTimeout:Int = 5 // seconds
): AutoCloseable {

	/** Queue of requests to run. */
	private val q:BlockingQueue<Req> = LinkedBlockingQueue()

	/** Flag to set when closed to stop queue poll loop. */
	private val closed = AtomicBoolean(false)

	/**
	 * Flag set when we start trying to connect, unset when disconnected.
	 * Used to determine if we should reconnect on controller error.
	 */
	private val connectingOrConnected = AtomicBoolean(false)
	/**
	 * Flag to set when connected, unset when disconnected.
	 * Used to determine if we should reconnect on controller error.
	 */
	private val connected = AtomicBoolean(false)

	/** Gate to block queue poll loop when not ready. */
	private val ready:Gate = Gate()

	/** The OBS controller. */
	val controller:OBSRemoteController = OBSRemoteController.builder()
		.host(host)
		.port(port)
		.password(password)
		.autoConnect(false)
		.connectionTimeout(connectionTimeout)
		.lifecycle()
			.onReady(::onReady)
			.onClose(::onClose)
			.onControllerError(::onControllerError)
			.onCommunicatorError(::onCommError)
			.onDisconnect(::onDisconnect)
			.onConnect {
				log.debug("connected")
				connected.set(true)
			}
			.and()
		.build()

	init {
		// OBSRemoteController starts a non-daemon thread. It probably should not do that.
		// Kill it on shutdown.
		addShutdownHook {
			close()
		}
		// Thread that connects if we are not connected/connecting.
		thread(name="obs-connect", isDaemon=true, start=true) {
			while(!closed.get()) {
				if (connectingOrConnected.compareAndSet(false,true)) {
					log.debug("Not closed, not connected. Try to connect...")
					try {
						// Only call connect from here; if you try to call connect from [onControllerError] or [onDisconnect]
						// eventually you will get stack overflow.
						controller.connect()
					} catch (e:Exception) {
						log.warn("failed to connect: ${e.message}", e)
					}
				}
				Thread.sleep(connectionTimeout.toLong() * 1000L) // in case the error was immediate
			}
		}
	}

	private fun onClose(e:WebSocketCloseCode) {
		log.debug("closed: ${e.code}")
		ready.close()
	}

	private fun onControllerError(e:ReasonThrowable) {
		log.debug("controller error - ${e.reason}",e.throwable)
		// If we are not connected, a controller error means that connection failed and we will not connect.
		// If we are connected, it does not mean we are/will be disconnected.
		if (!connected.get()) {
			connectingOrConnected.set(false)
		}
	}
	private fun onCommError(e:ReasonThrowable) {
		log.debug("comm error - ${e.reason}",e.throwable)
	}
	private fun onDisconnect() {
		log.debug("disconnected")
		connectingOrConnected.set(false)
		connected.set(false)
	}

	private fun onReady() {
		log.debug("ready")
		ready.open()
		// The docs say that you are only supposed to send requests from the [onReady] handler,
		// but you cannot block the [onReady] handler.
		// (If you block the [onReady] handler other handlers are not called. [onDisconnect] is not called so you
		// cannot reconnect if OBS is restarted.)
		// This would be fine if `onReady` was called after each response, but it is not.
		// So we keep track of if it is ready, and make requests from another thread ([opThread]).
	}

	/**
	 * Thread that runs submitted requests from [q] when [ready].
	 */
	private val opThread = thread(name="obs-op", isDaemon=false, start=true) {
		while(!closed.get()) {
			val req = try {
				q.take()
			} catch (e:InterruptedException) {
				log.debug("interrupted taking req")
				continue
			}
			log.debug("got req: ${req}, wait for ready")
			try {
				ready.enter {
					log.debug("ready")
					if (!req.expired()) {
							req.block.invoke(controller)
					}
				}
			} catch (e:Exception) {
				log.error("req ${req} failed", e )
			}
		}
		log.debug("done")
	}

	/**
	 * Submit a request to run when ready.
	 *
	 * @param timeout If this time has elapsed before ready, do not run. Always run if null.
	 * @param block the request to run
	 */
	fun submit(
		timeout:Duration? = null,
		block:(OBSRemoteController)->Unit,
	) {
		q.put(Req(block, timeout?.inWholeNanoseconds))
	}

	override fun close() {
		closed.set(true)
		opThread.interrupt()
		controller.disconnect()
		controller.stop()
	}

	companion object {
		private val log = LoggerFactory.getLogger(Obs::class.java)

		/**
		 * Wrap a request and keep track of timeout.
		 */
		private class Req(
			val block:(OBSRemoteController)->Unit,
			val timeout:Long?,
		) {
			val submitTime = System.nanoTime()
			fun expired():Boolean = timeout != null && System.nanoTime() - submitTime > timeout
		}
	}
}

