package net.eksb.obsdc

import org.freedesktop.dbus.annotations.DBusInterfaceName
import org.freedesktop.dbus.connections.impl.DBusConnection
import org.freedesktop.dbus.connections.impl.DBusConnectionBuilder
import org.freedesktop.dbus.exceptions.DBusException
import org.freedesktop.dbus.interfaces.DBusInterface
import org.freedesktop.dbus.interfaces.DBusSigHandler
import org.freedesktop.dbus.messages.DBusSignal
import org.slf4j.LoggerFactory

/**
 * Listen to signals on the session DBUS, and send the ops to [q].
 *
 * To send an op signal:
 * `dbus-send --session --type=signal --dest=net.eksb.obsdc / net.eksb.Obsdc.Signal string:{OP_NAME}`
 *
 * `{OP_NAME}` is the name of an [Op].
 *
 * `src/scripts/obsdc-signal` takes an [Op] name and sends the signal.
 */
class DBus(private val opHandler:(String)->Unit): AutoCloseable {

	// To monitor DBUS: `dbus-monitor`
  // To see what is registered: `qdbus net.eksb.obsdc /`

	val dbus = DBusConnectionBuilder.forSessionBus().build()

	init {
		// These lines are not necessary to handle signals, but are necessary to register methods.
		try {
			dbus.requestBusName(BUS_NAME)
		} catch (e:DBusException) {
			error("Error requesting bus. Already running?")
		}
		dbus.exportObject("/", ObsdcDBusInterfaceImpl())

		dbus.addSigHandler<ObsdcDBusInterface.Signal> { signal ->
			log.debug("signal: ${signal.op}")
			opHandler.invoke(signal.op)
		}
		log.debug("DBUS initialized")
	}

	override fun close() {
		dbus.releaseBusName(BUS_NAME)
		dbus.close()
	}

	companion object {
		private const val BUS_NAME = "net.eksb.obsdc"
		private val log = LoggerFactory.getLogger(DBus::class.java)
	}
}

inline fun <reified T: DBusSignal> DBusConnection.addSigHandler(handler: DBusSigHandler<T>) {
	addSigHandler(T::class.java, handler)
}

@DBusInterfaceName("net.eksb.Obsdc")
interface ObsdcDBusInterface: DBusInterface {
	fun echo(message:String): String
	class Signal(path:String, val op:String): DBusSignal(path, op)
}

class ObsdcDBusInterfaceImpl: ObsdcDBusInterface {
	override fun echo(message: String):String {
		return message
	}
	override fun getObjectPath(): String = "/"
}