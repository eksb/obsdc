package net.eksb.obsdc

import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

/**
 * A Gate blocks [enter] until the gate is opened by [open].
 */
class Gate {

	private val lock:Lock = ReentrantLock()
	private val open = AtomicBoolean(false)
	private val condition = lock.newCondition()

	/**
	 * Open the gate; allow all waiting [enter]s to run.
	 */
	fun open() {
		lock.lock()
		try {
			open.set(true)
			condition.signalAll()
		} finally {
			lock.unlock()
		}
	}

	/**
	 * Close the gate; any subsequent calls to [enter] will block until [open] is called.
	 */
	fun close() {
		open.set(false)
	}

	/**
	 * Enter the gate: run the specified [block] as soon as the gate is open.
	 */
	fun <R> enter(block:()->R): R {
		lock.lock()
		try {
			while(!open.get()) {
				condition.await()
			}
			return block()
		} finally {
			lock.unlock()
		}
	}
}