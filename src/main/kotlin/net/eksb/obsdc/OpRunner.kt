package net.eksb.obsdc

import io.obswebsocket.community.client.message.request.RequestBatch
import io.obswebsocket.community.client.message.request.sceneitems.GetSceneItemLockedRequest
import io.obswebsocket.community.client.message.response.sceneitems.GetSceneItemLockedResponse
import io.obswebsocket.community.client.model.Scene
import io.obswebsocket.community.client.model.SceneItem.Transform
import io.obswebsocket.community.client.model.SceneItem.Transform.TransformBuilder
import org.slf4j.LoggerFactory

/**
 * Use an [obs] to run an operation.
 *
 * Call [invoke] to run an OBS operation.
 */
class OpRunner(private val obs:Obs): (String)->Unit {

	/** How much to pan. */
	private val panAmount = 50F

	private val controller = obs.controller

	/**
	 * Run the specified [op].
	 *
	 * @param op Operation to run.
	 */
	override fun invoke(op:String) {
		obs.submit { controller ->
			when {
				op == "SCENE_NEXT" -> scene { scenes, current ->
					if (current != null) {
						scenes.asSequence()
							.dropWhile { scene -> scene != current }
							.drop(1)
							.firstOrNull()
							?: scenes.firstOrNull()
					} else {
						null
					}
				}
				op == "SCENE_PREV" -> scene { scenes, current ->
					if (current != null) {
						scenes.reversed().asSequence()
							.dropWhile { scene -> scene != current }
							.drop(1)
							.firstOrNull()
							?: scenes.lastOrNull()
					} else {
						null
					}
				}
				op.startsWith("SCENE_") -> scene { scenes, current ->
					val drop = op.drop("SCENE_".length).toInt() - 1
					scenes.asSequence().drop(drop).firstOrNull()
				}
				op == "STUDIO_TRANSITION" -> {
					controller.triggerStudioModeTransition { response ->
						log.debug("studio transitioned: ${response.isSuccessful}")
					}
				}
				op == "STUDIO_MODE_TOGGLE" -> studioModeToggle()
				op == "PAN_UP" -> transform { old -> positionY(old.positionY - panAmount ) }
				op == "PAN_DOWN" -> transform { old -> positionY(old.positionY + panAmount ) }
				op == "PAN_LEFT" -> transform { old -> positionX(old.positionX - panAmount ) }
				op == "PAN_RIGHT" -> transform { old -> positionX(old.positionX + panAmount ) }
				else -> log.error("Unhandled op \"${op}\"")
			}
		}
	}

	private fun studioModeToggle() {
		controller.getStudioModeEnabled { response ->
			if (response.isSuccessful) {
				val enable = !response.studioModeEnabled
				log.debug("toggle studio mode: ${!enable}")
				controller.setStudioModeEnabled(enable) { response ->
					log.debug("toggled studio mode: ${enable}")
				}
			}
		}
	}

	/**
	 * Select a scene from the scene list with the supplied [selector] and set the selected scene (if any)
	 * as the current program scene.
	 *
	 * @param selector Lambda that takes as arguments the list of scenes and the current scene (or null if the current
	 * scene is unknown), and returns the scene to select, or null to not change the scene.
	 */
	private fun scene(selector:(List<Scene>,Scene?)->Scene?) {
		controller.getCurrentProgramScene { response ->
			val currentSceneName = response.currentProgramSceneName
			controller.getSceneList { response ->
				val scenes = response.scenes.sortedBy(Scene::getSceneIndex).reversed()
				val currentScene = scenes.find { scene -> scene.sceneName == currentSceneName }
				val scene = selector(response.scenes.sortedBy(Scene::getSceneIndex).reversed(), currentScene)
				log.debug("select scene ${scene?.sceneName} index:${scene?.sceneIndex}")
				if (scene != null) {
					controller.setCurrentProgramScene(scene.sceneName) { response ->
						log.debug("selected scene ${scene.sceneName}: ${response.isSuccessful}")
					}
				}
			}
		}
	}

	/**
	 * Generate a transform for the lowest non-locked item in the current program scene with the
	 * supplied [transformBuilder], and apply that transform to the item.
	 */
	private fun transform(transformBuilder:TransformBuilder.(Transform)->TransformBuilder) {
		controller.getCurrentProgramScene { response ->
			val sceneName = response.currentProgramSceneName
			log.debug("scene name: ${sceneName}")
			controller.getSceneItemList(sceneName) { response ->
				val items = response.sceneItems
				// Even though locked status is in the response from OBS, the library does not parse it.
				// So we have to ask for it explicitly:
				controller.sendRequestBatch(
					RequestBatch.builder()
						.requests(
							response.sceneItems.map { item ->
								GetSceneItemLockedRequest.builder()
									.sceneName(sceneName)
									.sceneItemId(item.sceneItemId)
									.build()
							}
						)
						.build()
				) { response ->
					val item = response.data.results
						.map { result ->
							(result.responseData as GetSceneItemLockedResponse.SpecificData).sceneItemLocked
						}
						.zip(items)
						.asSequence()
						.filter { (locked,item) -> ! locked }
						.map { (locked,item) -> item }
						.sortedBy { item -> item.sceneItemIndex }
						.firstOrNull()
					log.debug("item to pan: ${item?.sceneItemId}")
					if (item != null) {
						controller.getSceneItemTransform(sceneName, item.sceneItemId) { response ->
							val transform = response.sceneItemTransform
							log.debug("position: ${transform.positionX} x ${transform.positionY}")
							val newTransform = transformBuilder(Transform.builder(), transform).build()
							controller.setSceneItemTransform(sceneName, item.sceneItemId, newTransform) { response ->
								log.debug("transform successful: ${response.isSuccessful}")
								// Have to set the current scene to take effect if in studio mode.
								controller.setCurrentProgramScene(sceneName) { response ->
									log.debug("set current program to ${sceneName}: ${response.isSuccessful}")
								}
							}
						}
					}
				}
			}
		}
	}

	companion object {
		private val log = LoggerFactory.getLogger(OpRunner::class.java)
	}
}

