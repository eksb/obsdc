package net.eksb.obsdc

import org.slf4j.LoggerFactory

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        try {
            val config = CONFIG_FILE.properties()
            Obs(
                host = config.getProperty("host") ?: "localhost",
                port = config.getProperty("port")?.toInt() ?: 4455,
                password = config.getProperty("password") ?: error("config missing \"password\""),
                connectionTimeout = config.getProperty("connectionTimeout")?.toInt() ?: 5
            ).use { obs ->
                DBus(OpRunner(obs)).use { dbus ->
                    waitForShutdown()
                }
            }
        } catch (e:Exception) {
            log.error(e.message, e)
        }
    }

    private val log = LoggerFactory.getLogger(Main::class.java)
}