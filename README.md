# obsdc

OBS control from D-Bus messages

Send a D-Bus signal to perform an operation with `src/obsdc-signal`, which takes a single operation command
as an argument, and performs an action in OBS.

## Operations

* `SCENE_NEXT` - switch to the next scene
* `SCENE_PREV` - switch to the previous scene
* `SCENE_#` - switch to the indicated scene
  * replace `#` with scene index, starting with 1.
* `STUDIO_TRANSITION` - transition the studio preview and program
* `STUDIO_MODE_TOGGLE` - toggle studio mode
* `PAN_UP` - move the top unlocked source up 50px
* `PAN_DOWN` - move the top unlocked source down 50px
* `PAN_LEFT` - move the top unlocked source left 50px
* `PAN_RIGHT` - move the top unlocked source right 50pxt

## Configuration

Configuration is read from `$XDG_CONFIG_HOME/net.eksb.obsdc/config.properties`

### Configuration options

* `host` - OBS websocket host (default: `localhost`)
* `port` - OBS websocket port (default: `4455`)
* `password` - OBS websocket password (required)

### systemd

A systemd unit file ([obsdc.service](src/main/dist/obsdc.service)) is included.

1. Copy the file to `~/.config/systemd/user/`.
2. Edit the `ExecStart` line for where you have installed obsdc.
3. `systemctl --user enable obsdc`
4. `systemctl --user start obsdc`

## Build

`gradle build`

## Run

1. `tar xf build/distributions/obsdc.tar`
2. `cd obsdc`
3. Create config file. See [#Configuration].
4. `bin/obsdc`
5. `bin/obsdc-signal` to send an op.

## Code

Start at [Main](src/main/kotlin/net/eksb/obsdc/Main.kt).

[Obs](src/main/kotlin/net/eksb/obsdc/Obs.kt) is a wrapper around
[obs-websocket-java](https://github.com/obs-websocket-community-projects/obs-websocket-java)
that handles reconnects and queuing operations until the websocket it ready.

[OpRunner](src/main/kotlin/net/eksb/obsdc/OpRunner.kt) has the logic to run [Op](src/main/kotlin/net/eksb/obsdc/Op.kt)s
using an `Obs`.

[DBus](src/main/kotlin/net/eksb/obsdc/DBus.kt) listens for the `D-Bus` signals and calls `OpRunner`.

## History/Rationale

I usually code on a 4k monitor in portrait mode. To show what I am doing on video calls,
I use OBS to share that monitor, but it is a 2160x4096 source in a 1920x1080 scene.

I have
[a macropad](https://keeb.io/collections/bdn9-collection/products/bdn9-rev-2-3x3-9-key-macropad-rotary-encoder-and-rgb)
with three knobs.
My [window manager](https://swaywm.org/) is configured to call `obsdc-signal` with
`PAN_UP`/`PAN_DOWN` for the keys emitted by the left knob,
`PAN_RIGHT`/`PAN_LEFT` for the keys emitted by the right knob, and
`SCENE_NEXT`/`SCENE_PREV` for the keys emitted by the center knob.
The regular keys on the macropad are mapped to the other actions.

So now I can quickly control what people can see without having to focus (or even make visible) the OBS window.
