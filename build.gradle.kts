plugins {
    id("org.jetbrains.kotlin.jvm") version "1.9.0"
    application
}

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.3")

    implementation("com.github.hypfvieh:dbus-java-core:4.3.1")
    runtimeOnly("com.github.hypfvieh:dbus-java-transport-native-unixsocket:4.3.1")

    implementation("io.obs-websocket.community:client:2.0.0")

    implementation("org.slf4j:slf4j-api:2.0.9")
    runtimeOnly("org.slf4j:slf4j-simple:2.0.9")
}

// Apply a specific Java toolchain to ease working on different environments.
java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

application {
    mainClass.set("net.eksb.obsdc.Main")
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}
